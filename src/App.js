import logo from './logo.svg';
import './App.css';

import CompShowBlogs from './blog/ShowBlogs';
import CompCreateBlog from './blog/CreateBlog';
import CompEditBlog from './blog/EditBlog';

import { BrowserRouter, Route, Routes } from 'react-router-dom';

import Keycloak from 'keycloak-js';
import { useEffect, useState } from 'react';

const keycloakOptions = {
  url: "https://keycloak-project-workflow-invoke.apps.m4j52foa.eastus.aroapp.io/auth/",
  realm: "testlogin",
  clientId: "test",
}

function App() {
  
  const [keycloak, setKeycloak] = useState(null)

  useEffect( ()=> {
    const initKeycloak = async () => {
      const keycloakInstance = new Keycloak(keycloakOptions)
      try{
        await keycloakInstance.init({onLoad: 'login-required'})
        setKeycloak(keycloakInstance)
        if(keycloakInstance.authenticated){
          console.log(keycloakInstance);
        }
      }catch (error){
        console.log(`Error ${error}`);
      }
    }
    initKeycloak()
  },[])
  
  const handleLogout = () => {
    if(keycloak){
      keycloak.logout()
    }
  }

  return (
    <div>
        <nav className="navbar navbar-expand-lg navbar-light bg-light">
          <div className="container-fluid">
            <a className="navbar-brand" href="#">Navbar</a>
            <button className="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
              <span className="navbar-toggler-icon"></span>
            </button>
            <div className="collapse navbar-collapse" id="navbarSupportedContent">
              <ul className="collapse navbar-collapse" id="navbarNav">
                {keycloak && keycloak.authenticated ? (  
                  <li className="nav-item">
                    <button className="btn btn-danger" onClick={handleLogout}>Cerrar Sesión</button>
                  </li>
                ) : null}   
              </ul>  
            </div>
          </div>
        </nav>

      <div className="App">
        {keycloak && keycloak.authenticated ? (
          <div>
            <header className="App-header">
            <img src={logo} className="App-logo" alt="logo" />
            </header>
              <h1>Bienvenido: {`${keycloak.tokenParsed.given_name}`}</h1>
              <BrowserRouter>
                <Routes>
                  <Route path='/' element={<CompShowBlogs />} />
                  <Route path='/create' element={<CompCreateBlog />} />
                  <Route path='/edit/:id' element={<CompEditBlog />} />
                </Routes>
              </BrowserRouter>
          </div>
        ) : null}
      </div>
    </div>
  );
}

export default App;
